import axios from 'axios';
import {setAlert} from "./alert"
import {
    UPDATE_VOTES,VIDEO_LOAD,ADD_ERROR,SET_LOADING,Add_COTESTANT,POST_ERROR,ADD_POLL,POLLS_LOADED,AUTH_ERROR,POLLS_ITEM_LOADED
} from './types'
import { body } from 'express-validator';

export const addContestant =({contestantName,city,image,id}) => async dispatch => {

  var formData = new FormData();

  formData.append('contenstantName',contestantName);
  formData.append('city',city);
  formData.append('image',image);

  
  try{
    dispatch({
      type:SET_LOADING
    })
    const res=await axios.post(`/api/poll/pollist/${id}`,formData);
   
    dispatch({
      type:Add_COTESTANT,
      payload:res.data
    });
    dispatch(setAlert("Successfully Added Contestant",'success'));

  }catch(err){
    const errors = err.response.data.errors;
    console.log(errors);
    if (errors) {
      errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
    }
    if(err.response.msg){
      dispatch(setAlert(err.response.msg,"danger"));
    }
    dispatch({
      type:ADD_ERROR
    });

  }
}
export const addVideo =({image}) => async dispatch => {

  var formData = new FormData();

 
  formData.append('image',image);

  
  try{
    dispatch({
      type:SET_LOADING
    })
    const res=await axios.post(`/api/poll/videoupload`,formData);
   
    dispatch({
      type:Add_COTESTANT,
      payload:res.data
    });
    dispatch(setAlert("Successfully Added Video",'success'));

  }catch(err){
    const errors = err.response.data.errors;
    console.log(errors);
    if (errors) {
      errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
    }
    if(err.response.msg){
      dispatch(setAlert(err.response.msg,"danger"));
    }
    dispatch({
      type:ADD_ERROR
    });

  }
}
export const addVote = (id1) => async dispatch => {
 

    try {
     
      const res = await axios.put(`/api/poll/vote/${id1}`);
       
      dispatch({
        type: UPDATE_VOTES,
        payload: { id1, votes: res.data }
      });
      dispatch(setAlert("Thanks for Voting",'success'));
    } catch (err) {
      const errors = err.response.data.errors;
      
      if (errors) {
        errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
      }      dispatch({
        type: POST_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status }
      });
    }
  };

  export const addPoll = pollname => async dispatch =>{
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    };
    
    const body = JSON.stringify({ pollname });
     
   try{
     
    const res = await axios.post(`/api/poll`,body,config);
    dispatch({
      type:ADD_POLL,
      payload:res.data
    });
    dispatch(setAlert("Successfully Added Poll",'success'));
   }catch(err){
    
   }
     
    
  }
  export const loadPollsList =() => async dispatch => {
    
    try{
      const res=await axios.get('api/poll');
      dispatch({
        type:POLLS_LOADED,
        payload:res.data
      });
    }catch(err){
      dispatch({
        type:AUTH_ERROR
      });
  
    }
  }
  export const loadVideos =() => async dispatch => {
    
    try{
      const res=await axios.get('/api/poll/video/data');
      dispatch({
        type:VIDEO_LOAD,
        payload:res.data
      });
    }catch(err){
      dispatch({
        type:AUTH_ERROR
      });
  
    }
  }
 
  export const loadPollsItem =(id) => async dispatch => {
    
    try{
      const res=await axios.get(`/api/poll/${id}`);
      
      dispatch({
        type:POLLS_ITEM_LOADED,
        payload:res.data
      });
    }catch(err){
      dispatch({
        type:AUTH_ERROR
      });
  
    }
  }