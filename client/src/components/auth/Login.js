import React, {useState,useEffect} from 'react';
import {login} from '../../actions/auth'
import {Link,Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import Alert from '../layout/Alert'
import Backdrop from '../backdrop/BackDropLoad';
import Spinner from '../layout/Spinner'
const Login = ({login,isAuthenticated,loading}) =>{

    const [formdata,setFormData]=useState({
       
        email:"",
        password:""
       
    });

    const [hidden,setHidden]=useState(true);

    
const { email, password}=formdata;
const onChange = e => setFormData({...formdata, [e.target.name]:e.target.value});

const onSubmit = async e => {
 e.preventDefault();
    
 
       
  onLoad();
  

    login({email,password});
    
    
}
const onLoad =()=>setHidden(false);
const onLoads = () =>setHidden(true);

if(isAuthenticated){
  return <Redirect to="/polls"></Redirect>

}


  return(
    
    <>
    
     

      {!hidden&&<div onClick={()=>onLoads()}><Backdrop /><Spinner/></div>}

      
   
    <section className="container">
     <div className="alertDiv">
         {loading?<Spinner/>:<div className="row col-md-4 col-12 mt-2 offset-md-4"><Alert/></div>}
        </div> 
      <h1 className="large text-primary">Log in</h1>
      <p className="lead"><i className="fas fa-user"></i></p>
      <form className="form" onSubmit={e=>onSubmit(e)}>
       
        <div className="form-group">
          <input type="email" placeholder="Email Address" name="email" value={email} onChange={e=>onChange(e) }/>
          <small className="form-text"></small >
        </div>
        <div className="form-group">
          <input
            type="password"
            placeholder="Password"
            name="password"
            minLength="6" value={password} onChange={e=>onChange(e)}
          />
        </div>
        
        <input type="submit" className="btn btn-primary" value="Log in" />
      </form>
      <p className="my-1">
        Dont have an account? <Link to="/register">Sign Up</Link>
      </p>
    </section>
 </>
  )
  

}
const mapStateToProps = state =>{
  return{
    isAuthenticated:state.auth.isAuthenticated,
    loading:state.auth.loading
  }
}
export default connect(mapStateToProps,{login})(Login)
