import React,{useState} from 'react';
import {connect} from 'react-redux';
import {addContestant} from '../../actions/poll';
import Backdrop from '../backdrop/BackDropLoad'
import Alert from '../layout/Alert';
import Spinner from '../layout/Spinner'
const AddContestant = ({loading,addContestant,match}) =>{

    const [formData,setFormData]=useState({
        contestantName:"",
        city:"",
        image:null
    })

    const {contestantName,city,image}=formData;

    const onSubmit = async e =>{
        e.preventDefault();
        let id=match.params.id;
        addContestant({contestantName,city,image,id});
        
    }
const onFileSubmit=(e)=>{

    setFormData({...formData,image: e.target.files[0]})
    
}
    

   
  const onChange = e =>{
  setFormData({ ...formData, [e.target.name]: e.target.value });
}

  return(<>
  {
   !loading&&<><Backdrop/></>}
<div style={{height:'40px'}}> {!loading?<Spinner/>:<div className="row col-md-4 col-12 mt-2 offset-md-4"><Alert/></div>}</div>
   

   <div className="row mt-5">

       <div className="col-md-4 offset-md-4 px-4">
       <div className="card">
      
       <div className="card-body">

           <form onSubmit={e=>onSubmit(e)} method="post" encType="multipart/form-data">
               <div class="form-group">
                   <label className="card-text">Contestant Name</label>
                   <input type="text" name="contestantName" value={contestantName} onChange={e=>onChange(e)} className="form-control" required/>
               </div>
               <div className="form-group">
                   <label className="card-text">City</label>
                   <input type="text" name="city" value={city} onChange={e=>onChange(e)} className="form-control"/>
               </div>
               <div class="custom-file mb-3">
                   <input type="file" class="custom-file-input" name="image" onChange={e=>onFileSubmit(e)} id="customFile"/>
                   <label className="custom-file-label" htmlFor="customFile">Choose file</label>
               </div>
               <div className="form-group">
                   <input type="submit" value="Add Contestant" className="btn btn-primary"/>
               </div>
           </form>
       </div>
      
   </div>
       </div>

   </div>

</>
  )

}

const mapStateToProps = state =>{
return{
    loading:state.poll.loading
}
}

export default connect(mapStateToProps,{addContestant})(AddContestant)