import React,{useState} from 'react';
import {connect} from 'react-redux';
import {addVideo} from '../../actions/poll';
import Backdrop from '../backdrop/BackDropLoad'
import Alert from '../layout/Alert';
import Spinner from '../layout/Spinner'
const AddVideo = ({loading,addVideo}) =>{

    const [formData,setFormData]=useState({
      
        image:null
    })

    const {image}=formData;

    const onSubmit = async e =>{
        e.preventDefault();
       
        addVideo({image});
        
    }
const onFileSubmit=(e)=>{

    setFormData({...formData,image: e.target.files[0]})
    
}
    

   
  

  return(<>
  {
   !loading&&<><Backdrop/></>}
<div style={{height:'40px'}}> {!loading?<Spinner/>:<div className="row col-md-4 col-12 mt-2 offset-md-4"><Alert/></div>}</div>
   

   <div className="row mt-5">

       <div className="col-md-4 offset-md-4 px-4">
       <div className="card">
      
       <div className="card-body">

           <form onSubmit={e=>onSubmit(e)} method="post" encType="multipart/form-data">
              
               <div class="custom-file mb-3">
                   <input type="file" class="custom-file-input" name="image" onChange={e=>onFileSubmit(e)} id="customFile"/>
                   <label className="custom-file-label" htmlFor="customFile">Choose file</label>
               </div>
               <div className="form-group">
                   <input type="submit" value="Video Upload" className="btn btn-primary"/>
               </div>
           </form>
       </div>
      
   </div>
       </div>

   </div>

</>
  )

}

const mapStateToProps = state =>{
return{
    loading:state.poll.loading
}
}

export default connect(mapStateToProps,{addVideo})(AddVideo)