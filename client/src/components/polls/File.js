import React,{useState} from 'react';
import {connect} from 'react-redux';

import {addImage} from '../../actions/post'
const File = ({addImage}) =>{
    const [formdata,setFormData]=useState({
       
        file:null
        
       
    });
    const {file} = formdata;
    const onChange = e => setFormData({...formdata, [e.target.name]:e.target.value});
    const onSubmit = async e => {
      e.preventDefault();
      
      addImage(file);
          
      }
  return(
    <div>
        <form onSubmit={e=>onSubmit(e)}>
        <input type="file" className="form-control mx-10" name="file" value={file} onChange={e=>onChange(e)} />
    

            <input type='submit' value='upload'/>
        </form>
    </div>
  )

}

export default connect(null,{addImage})(File)