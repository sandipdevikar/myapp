import React, { useEffect } from 'react';
import {Redirect } from 'react-router-dom';
//import paras from '../../img/paras.jpeg';
import {addVote, loadPollsItem} from "../../actions/poll";
import {connect} from "react-redux";
import Alert from '../layout/Alert';
import uuid from 'uuid';
import Backdrop from '../backdrop/BackDropLoad'
import Spinner from '../layout/Spinner'
import {setAlert} from '../../actions/alert';
const Poll = ({addVote,loadPollsItem,setAlert1,isAuthenticated,loading,poll,pollslist,alerts,match}) =>{
useEffect(()=>{
        console.log("load");
        loadPollsItem(match.params.id)
       
     
},[]);
        
        const addVoteIn =(id1,id2) =>{
               
                if(isAuthenticated){
                        try {
                               
                                addVote(id1+"/"+id2);
                        } catch (error) {
                                
                        }

                }else{
                        
                
                 setAlert1("Not yet login", 'danger');
                      return  <Redirect to='/login'></Redirect>
                }

        }
        var url;
  return(
          loading?<><Spinner/></>:<>
         
          
          <>
            
         
       </>
       <div class="container-fluid vote-body">
                           <div class="row">
                                   <div class="col-md-12">
                                   
                                           <div class="row">
                                           
                                                   <div class="col-md-2">
   
                                                   </div>
                                                   <div class="col-md-8">
                                                           <div class="card topic-heading-card col-margin">
                                                            <label class="topic-heading">{poll.pollname}</label>
                                                         
                                                            <Alert></Alert>
   
                                                           
                                                           
                                                           
                                                           
                                                   </div>
                                                   </div>
                                                   <div class="col-md-2">
                                                           
                                                   </div>
   
                                           </div>        
                                    </div>
                                    <div class="col-md-12">
                                            <div class="row">
                                                   
                                                { poll.pollslist != null&&poll.pollslist.length>0&&
                           poll.pollslist.map(item=>(
   
                           
                                 <>
                                 
                                 <div class="col-md-6 col-12 "  key={item._id}>
                                                                   <div class="card col-margin">
                                                                            <div class="row">
                                                                                            <div class="col-md-3 col-4 pr-0">
                                                                                                            <div class="card applicant-img-card">
                                                                                                                   {
                                                                                                                   
                                                                                                                   }
                                                                                                                            <img src={`${item.imgURL}`} class="applicant-img" alt="sid image" ></img>
                                                                                                                       
                                                                                                            </div>
                                                                                            </div> 
                                                                                            <div class="col-md-6 col-5 pl-0 pr-0">
                                                                                                            <div class="card">
                                                                                                                           <label class="contestant-name">{item.contenstantName}</label>
                                                                                                                           <label class="contestant-location">{item.city}</label>
                                                                                                            </div>         
                                                                                           </div>
                                                                                           <div class="col-md-3 col-3 pl-0">
                                                                                                           <div class="card">
                                                                                                                  
                                                                                                                   <button class="vote-button" onClick={(e)=>addVoteIn(poll._id,item._id)}>Vote</button>              
                                                                                                            </div>
                                                                                            </div>  
                                                                           </div>  
                                                                    </div>
                                                           </div>    
                                 
                                 </>
                             
                           ))
   
                                                }     
                                                        
                                                                   
                                            </div>
                                    </div>      
   
                                            
           
                           </div>
                    </div>
   
          
          
          </>
  
    )

}
const mapStateToProps = state =>{
        return {
                alerts:state.alerts,
                isAuthenticated:state.auth.isAuthenticated,
                poll:state.poll.poll,
                loading:state.poll.loading
    
        }
}
const mapDispatchToProps = dispatch =>{
        const id= uuid.v4();
return {
        loadPollsItem:(id)=>dispatch(loadPollsItem(id)),
        addVote:(id)=>dispatch(addVote(id)),
        setAlert1:(msg,alertType)=>dispatch((setAlert(msg,alertType)))
}
}
export default connect(mapStateToProps,mapDispatchToProps)(Poll)