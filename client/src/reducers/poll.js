import { UPDATE_VOTES,Add_COTESTANT,VIDEO_LOAD,ADD_ERROR,POST_ERROR,SET_LOADING,ADD_POLL,POLLS_LOADED,POLLS_ITEM_LOADED
    } from "../actions/types";
  
  const initialState = {
    polls: [],
    poll: [],
    loading: true,
    error: {},
    video:[]
  };
  
  export default function(state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      case VIDEO_LOAD:
        return{
          ...state,
          video:payload,
          loading:false
        }
      
      case ADD_ERROR:
        return{
          loading:true
        }
      case SET_LOADING:
        return{
          loading:false
        }
        case Add_COTESTANT:
          return{
            loading:true
          }
      case UPDATE_VOTES:
        return {
          ...state,
          polls: state.polls.map(poll =>
            poll._id === payload.id ? { ...poll, votes: payload.votes } : poll
          ),
          loading: false
        };
        case ADD_POLL:
          return{
              ...state,
              polls:payload,
              loading:false
          };
          case POLLS_ITEM_LOADED:
            return{
              ...state,
              poll:payload,
              loading:false
            }
          case POLLS_LOADED:
            return{
              ...state,
              polls:payload,
              loading:false
            }
        case POST_ERROR:
          return{
              ...state,
              error:payload,
              loading:false

        }
       default:
        return state;
    }
  }
  