const express = require("express");
const multer = require('multer');
const router = express.Router();
const auth = require('../../middleware/auth');
const Poll = require("../../models/poll");
const User = require("../../models/user");
const uploads = require("../../models/File");
const Contestant = require("../../models/contestant");
const {check,validationResult} = require("express-validator/check");
const Video = require("../../models/video")
const storage = multer.diskStorage({
  destination:function(req,file,cb){
cb(null,'images/')
  },
  filename:function(req,file,cb){
    cb(null,file.originalname);
  }

});
const upload = multer({
  storage:storage,
  limits:{
    fileSize: 50000000000
  },
 
})

router.post('/',async (req,res)=>{

  const {pollname} = req.body;
try{

  let pollnames = new Poll({
    pollname
  })
  await pollnames.save();
  res.json(pollnames)
}catch(err){
console.log(err.message);
return res.status(500).json({msg:"server error"});
}

});
router.post(
  '/pollist/:id',
  upload.single('image'),

    [
      check('contenstantName', 'name is required')
        .not()   
        .isEmpty()
       
    ] ,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({msg:errors.array()});
    }

    try {

      const {contenstantName} = req.body;
      


      let poll = await Poll.findById(req.params.id);
      console.log(poll);
     
      if ( poll.pollslist.filter(pollslist1 => 

pollslist1.contenstantName.toString() === 

req.body.contenstantName).length > 0 ) {
  return res.status(400).json({ errors: [{ msg: 'Contestant Already exist' }] });
      } 
      await poll.pollslist.unshift({contenstantName:req.body.contenstantName,city:req.body.city,imgURL:req.file.path});
         await poll.save();

      res.json(poll);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);


router.put('/vote/:id1/:id2', auth, async (req, res) => {
    try {
      const poll = await Poll.findById(req.params.id1);
   console.log(poll);
      const pollby=await poll.pollslist.find(obj =>{return  obj._id.toString() ===req.params.id2});
      
      let flag=1;
      const a =poll.pollslist.filter(ob =>{
        if(ob.votes.filter(vo=> vo.user.toString()==req.user.id).length>0){
          flag=0;
          return res.status(400).json({ errors: [{ msg: 'You have already vote' }] });

        };
      })
      // Check if the post has already been liked
     if(flag==1){
      
    await  pollby.votes.unshift({ user: req.user.id });
  
    await poll.save();

    res.json(poll);
     }
    } catch (err) {
      console.error(err.message);
   return res.status(500).send('Server Error');
    }
  });
router.get('/',async (req,res)=>{
  try {
   
    const polls= await Poll.find();
     res.json(polls)
 } catch (err) {
     console.log(err.message);
     return res.status(500).json({msg:"server error"});
 
 }    
});

router.get('/:id',async (req,res)=>{
  try {
    const polls= await Poll.findById(req.params.id);
   
     res.json(polls)
 } catch (err) {
     console.log(err.message);
     return res.status(500).json({msg:"server error"});
 
 }    
});

router.post("/addContestant",[
  
  check('name', 'name is requireds')
  .not()
  .isEmpty(),
  check('image', 'image is requireds')
  .not()
  .isEmpty()

],upload.single('image') ,async (req,res)=>{
      const errors=validationResult(req);
      if(!errors.isEmpty()){
         return res.status(400).json({msg : errors.array()});
      }
      const {
          name,
          city
          
      }=req.body;

      const image = req.file.path;
      console.log(image);
      try {
          let contestants= await Contestant.findOne({name});
          if (contestants) {
              return res
                .status(400)
                .json({ errors: [{ msg: 'User already exists' }] });
            }
          
            contestants = new Contestant({
               name,city,imgURL:image
           });
          
            await contestants.save();
            
            res.json(contestants)
      } catch (err) {
          console.error(err);
        return  res.status(500).send('Server error');  
      }
  
})


router.get('/contestant/:id',async (req,res)=>{
  try {
    const contestant= await Contestant.findById(req.params.id);
     res.json(contestant)
 } catch (err) {
     console.log(err.message);
     return res.status(500).json({msg:"server error"});
 
 }    
});

router.post('/videoupload',upload.single('image'),async(req,res)=>{
try{
  
  let vid=  await Video.find();
  
    vid = new Video({
      video:req.file.path
    })
    await vid.save()

res.json(vid);
}catch(err){
return res.status(400).json({msg:"server error"})
}
});

router.get('/video/data', async(req,res)=>{
  try{
   
    let vid=  await Video.find();
   
    res.json(vid)
  }catch(err){
    return res.status(400).json({msg:"server errors"})

  }
});
module.exports=router;